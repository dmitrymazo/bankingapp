//
//  Card.swift
//  BankingApp
//
//  Created by Dmitry Mazo on 3/12/20.
//  Copyright © 2020 Dmitry Mazo. All rights reserved.
//

import Foundation

final class BankCard {
    var id: String?
    var name: String
    var type: CardType
    var last4Numbers: String
    var provider: CardProvider
    var currentBallance: Money
    var lastOperation: Money?
    
    init(name: String,
         type: CardType,
         last4Numbers: String,
         provider: CardProvider,
         currentBallance: Money
    ) {
        self.name = name
        self.type = type
        self.last4Numbers = last4Numbers
        self.provider = provider
        self.currentBallance = currentBallance
    }
}

enum CardType {
    case debet
    case credit
}

enum CardProvider {
    case visa
    case mastercard
}

enum Currency {
    case usd
    case eur
    case rur
}

struct Money {
    var amount: Double
    var currency: Currency
}
