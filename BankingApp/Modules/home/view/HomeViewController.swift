//
//  HomeViewController.swift
//  BankingApp
//
//  Created by Dmitry Mazo on 3/10/20.
//  Copyright (c) 2020 Dmitry Mazo. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class HomeViewController: UIViewController {
    
    struct Constants {
        static let labelFontSize: CGFloat = 16
        static let cardsReuseId = "cardCellId"
    }
    
    // MARK: - Public properties -
    
    var presenter: HomePresenterInterface?
    
    // MARK: - Private properties -
    
    private var cardsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: Constants.labelFontSize)
        label.text = "Cards"
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var cardsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 0.93, green: 0.97, blue: 0.45, alpha: 1.0)
        view.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: Constants.cardsReuseId)
        view.isScrollEnabled = false
        
        return view
    }()
    
    private var cards = [BankCard]()
    
    // MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .orange
        view.addSubview(cardsLabel)
        view.addSubview(cardsCollectionView)
        cardsCollectionView.delegate = self
        cardsCollectionView.dataSource = self
        setupConstraints()
        
        presenter?.getCards { [weak self] cards in
            self?.cards = cards
            self?.cardsCollectionView.reloadData()
        }
    }
    
    // MARK: - Private -
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            cardsLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            cardsLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 5),
            cardsCollectionView.topAnchor.constraint(equalTo: cardsLabel.bottomAnchor, constant: 20),
            cardsCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 5),
            cardsCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -5),
            cardsCollectionView.heightAnchor.constraint(equalToConstant: 200),
        ])
    }
    
    // MARK: - Init -
    
//    init() {
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }

    
}

// MARK: - Extensions -

extension HomeViewController: HomeViewInterface {
}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: 100)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cardsReuseId, for: indexPath) as? CardCollectionViewCell else { return UICollectionViewCell() }
        let card = cards[indexPath.row]
        cell.configure(forCard: card)
        
        return cell
    }
    
}
