//
//  CardCollectionViewCell.swift
//  BankingApp
//
//  Created by Dmitry Mazo on 3/14/20.
//  Copyright © 2020 Dmitry Mazo. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    private var cardImageView: UIImageView = {
        let view = UIImageView()
        let image = UIImage()
        view.image = image
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }()
    
    private var name: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var last4Numbers: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var lastOperationAmount: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var amount: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    func configure(forCard card: BankCard) {
        
        let cardImageName: String
        switch card.provider {
        case .visa:
            cardImageName = "visa-card"
        case .mastercard:
            cardImageName = "mastercard-card"
        }
        
        if let image = UIImage(named: cardImageName) {
            cardImageView.image = image
        }
        name.text = card.name
        last4Numbers.text = "* \(card.last4Numbers)"
//        if let lastOperationM = card.lastOperation {
        lastOperationAmount.text = StringRepresenter.getString(forMoney: Money(amount: -453, currency: .rur))// lastOperationM)
//        }
        amount.text = StringRepresenter.getString(forMoney: card.currentBallance)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(cardImageView)
        addSubview(name)
        addSubview(last4Numbers)
        addSubview(lastOperationAmount)
        addSubview(amount)
        
        let topBottomMargin: CGFloat = 30
        let leftRightMargin: CGFloat = 20
        let labelToImageDistance: CGFloat = 15
        let minVerticalSpacing: CGFloat = 5
        
        NSLayoutConstraint.activate([
            cardImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: leftRightMargin),
            cardImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            cardImageView.widthAnchor.constraint(equalToConstant: 60),
            //
            name.topAnchor.constraint(equalTo: topAnchor, constant: topBottomMargin),
            name.leadingAnchor.constraint(equalTo: cardImageView.trailingAnchor, constant: labelToImageDistance),
            last4Numbers.leadingAnchor.constraint(equalTo: cardImageView.trailingAnchor, constant: labelToImageDistance),
            last4Numbers.topAnchor.constraint(greaterThanOrEqualTo: name.bottomAnchor, constant: minVerticalSpacing),
            last4Numbers.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -topBottomMargin),
            //
            lastOperationAmount.topAnchor.constraint(equalTo: topAnchor, constant: topBottomMargin),
            lastOperationAmount.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -leftRightMargin),
            lastOperationAmount.leadingAnchor.constraint(greaterThanOrEqualTo: name.trailingAnchor, constant: 10),
            amount.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -leftRightMargin),
            amount.leadingAnchor.constraint(greaterThanOrEqualTo: last4Numbers.trailingAnchor, constant: 10),
            amount.topAnchor.constraint(greaterThanOrEqualTo: lastOperationAmount.bottomAnchor, constant: minVerticalSpacing),
            amount.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -topBottomMargin),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

final class StringRepresenter {
    static func getString(forMoney money: Money) -> String {
        return "\(money.amount) \(StringRepresenter.getString(forCurrency: money.currency))"
    }
    
    private static func getString(forCurrency currency: Currency) -> String {
        switch currency {
        case .usd:
            return "usd"
        case .eur:
            return "eur"
        case .rur:
            return "rur"
        }
    }
}
